package com.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author hui
 * @QQGroup 819240525
 * @create 2019/7/21 8:53
 */
@SpringBootApplication
public class WebSocket {
    public static void main(String[] args) {
        SpringApplication.run(WebSocket.class,args);
    }
}
