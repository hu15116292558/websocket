package com.websocket.handler;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * @Author hui
 * @QQGroup 819240525
 * @create 2019/7/21 8:55
 */
@Component
public class MyHandler extends TextWebSocketHandler {
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

        System.out.println("已收到客户端数据...  "+message.getPayload());

        //向客户端发送消息
        session.sendMessage(new TextMessage("服务端消息已收到..."));

        if (message.getPayload().equals("3")){
            for (int i = 0; i < 3; i++){
                //向客户端发送消息
                session.sendMessage(new TextMessage("服务端消息已收到  "+i));
                Thread.sleep(2000);
            }
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {

        //建立连接
        String id = (String) session.getAttributes().get("id");
        session.sendMessage(new TextMessage(id+",  websocket欢迎您的光临..."));
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        System.out.println("websocket断开连接...");
    }
}
