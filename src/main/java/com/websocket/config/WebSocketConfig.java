package com.websocket.config;

import com.websocket.handler.MyHandler;
import com.websocket.handler.MyHandshakeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * @Author hui
 * @QQGroup 819240525
 * @create 2019/7/21 9:14
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private MyHandler myHandler;

    @Autowired
    private MyHandshakeInterceptor myHandshakeInterceptor;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

        registry.addHandler(this.myHandler,"/ws")
                .setAllowedOrigins("*")//开启跨域
                .addInterceptors(this.myHandshakeInterceptor);//Interceptor
    }
}
